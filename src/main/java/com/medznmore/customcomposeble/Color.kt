package com.medznmore.customcomposeble

import androidx.compose.ui.graphics.Color

internal val Neutral100 = Color(0xFFE4E7EB)
internal val TextSecondary = Color(0xFF666666)
internal val Error700 = Color(0xFFBF0404)
