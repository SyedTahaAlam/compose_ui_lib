package com.medznmore.customcomposeble.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import com.medznmore.customcomposeble.R
import java.util.Locale

@Composable
fun MnmButton(
    modifier: Modifier = Modifier,
    text: String,
    onClick: () -> Unit,
    enabled: Boolean = true,
    showLoading: Boolean = false,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        disabledBackgroundColor = MaterialTheme.colors.primary.copy(alpha = 0.5f)
    ),
    shape: Shape = RoundedCornerShape(0.dp),
    border: BorderStroke? = null,
    fontSize: TextUnit = TextUnit.Unspecified,
    showContent: Boolean = false,
    loaderColor: Color = MaterialTheme.colors.onPrimary

) {
    Button(
        modifier = modifier
            .height(dimensionResource(id = R.dimen.button_height))
            .fillMaxWidth(),
        onClick = onClick,
        shape = shape,
        enabled = enabled,
        elevation = ButtonDefaults.elevation(
            defaultElevation = dimensionResource(id = R.dimen.elevation_0),
            pressedElevation = dimensionResource(id = R.dimen.elevation_0)
        ),
        colors = colors,
        border = border,

        ) {
        if (showLoading) {
            CircularProgressIndicator(
                Modifier
                    .padding(dimensionResource(id = R.dimen.padding_0_25x))
                    .fillMaxHeight()
                    .aspectRatio(1f),
                color = loaderColor,
                strokeWidth = 3.dp
            )
        } else {

                Text(text = text.uppercase(Locale.getDefault()), fontSize = fontSize)

        }
    }
}

@Preview
@Composable
private fun PreviewMnmButton() {
    MnmButton(
        text = "Button",
        onClick = {},
        showLoading = true
    )
}