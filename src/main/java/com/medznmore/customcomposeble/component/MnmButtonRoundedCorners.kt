package com.medznmore.customcomposeble.component

import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.medznmore.customcomposeble.R
import java.util.Locale

@Composable
fun MnmButtonRoundedCorners(
    modifier: Modifier = Modifier,
    text: String,
    onClick: () -> Unit,
    enabled: Boolean = true,
    showLoading: Boolean = false
) {
    Button(
        modifier = modifier
            .height(dimensionResource(id = R.dimen.button_height))
            .fillMaxWidth(),
        onClick = onClick,
        shape = MaterialTheme.shapes.small,
        enabled = enabled,
        elevation = ButtonDefaults.elevation(
            defaultElevation = dimensionResource(id = R.dimen.elevation_0),
            pressedElevation = dimensionResource(id = R.dimen.elevation_0)
        ),
        colors = ButtonDefaults.buttonColors(
            disabledBackgroundColor = MaterialTheme.colors.primary.copy(alpha = 0.5f)
        )
    ) {
        if (showLoading) {
            CircularProgressIndicator(
                Modifier
                    .padding(dimensionResource(id = R.dimen.padding_0_5x))
                    .fillMaxHeight()
                    .aspectRatio(1f),
                color = MaterialTheme.colors.onPrimary,
                strokeWidth = 3.dp
            )
        } else {
            Text(text = text.uppercase(Locale.getDefault()))
        }
    }
}

@Preview
@Composable
private fun PreviewMnmButtonRoundedCorners() {
    MnmButtonRoundedCorners(
        text = "Button",
        onClick = {},
        showLoading = true
    )
}