package com.medznmore.customcomposeble.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.medznmore.customcomposeble.Error700
import com.medznmore.customcomposeble.Neutral100
import com.medznmore.customcomposeble.R
import java.util.Locale

@Composable
fun MnmOutlinedButtonSmall(
    modifier: Modifier = Modifier,
    text: String,
    onClick: () -> Unit,
    enabled: Boolean = true,
    textColor: Color = MaterialTheme.colors.primary,
    strokeColor: Color = Neutral100,
) {
    MnmOutlinedButton(
        modifier = modifier
            .height(dimensionResource(id = R.dimen.button_height_small)),
        text = text,
        onClick = onClick,
        enabled = enabled,
        textColor = textColor,
        strokeColor = strokeColor,
        textSize = 12.sp,
        contentPadding = PaddingValues(
            dimensionResource(id = R.dimen.padding_1_5x),
            dimensionResource(id = R.dimen.padding_1x)
        )
    )
}

@Composable
fun MnmOutlinedButton(
    modifier: Modifier = Modifier,
    text: String,
    onClick: () -> Unit,
    enabled: Boolean = true,
    textColor: Color = MaterialTheme.colors.primary,
    strokeColor: Color = Neutral100,
) {
    MnmOutlinedButton(
        modifier = modifier
            .height(dimensionResource(id = R.dimen.touch_target_size)),
        text = text,
        onClick = onClick,
        enabled = enabled,
        textColor = textColor,
        strokeColor = strokeColor,
        textSize = 12.sp,
        contentPadding = PaddingValues(
            dimensionResource(id = R.dimen.padding_3x),
            dimensionResource(id = R.dimen.padding_1_5x)
        )
    )
}

@Composable
private fun MnmOutlinedButton(
    modifier: Modifier = Modifier,
    text: String,
    onClick: () -> Unit,
    enabled: Boolean,
    textColor: Color,
    strokeColor: Color,
    textSize: TextUnit,
    contentPadding: PaddingValues
) {
    OutlinedButton(
        modifier = modifier.wrapContentWidth(),
        onClick = onClick,
        enabled = enabled,
        elevation = ButtonDefaults.elevation(
            defaultElevation = dimensionResource(id = R.dimen.elevation_0),
            pressedElevation = dimensionResource(id = R.dimen.elevation_0)
        ),
        contentPadding = contentPadding,
        border = BorderStroke(1.dp, strokeColor)
    ) {
        Text(
            text = text.uppercase(Locale.getDefault()),
            style = MaterialTheme.typography.button.copy(
                color = textColor,
                fontSize = textSize
            )
        )
    }
}

@Preview
@Composable
fun PreviewPickerOutlinedButtonSmall() {
    MnmOutlinedButtonSmall(
        text = "Log out",
        onClick = {},
        textColor = Error700,
        strokeColor = Neutral100
    )
}