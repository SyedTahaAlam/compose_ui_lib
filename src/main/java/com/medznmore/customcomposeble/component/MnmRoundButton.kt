package com.medznmore.customcomposeble.component

import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.medznmore.customcomposeble.R

@Composable
fun MnmRoundButton(
    radius: Dp = 100.dp,
    text: String,
    onClick: () -> Unit,
    textStyle: TextStyle = MaterialTheme.typography.h5.copy(
        fontWeight = FontWeight.Bold,
        lineHeight = 40.sp
    )
) {

    val diameter = radius * 2f

    val infiniteTransition = rememberInfiniteTransition()
    val outerCircleSize by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = diameter.value,
        animationSpec = infiniteRepeatable(
            animation = tween(1500, easing = FastOutSlowInEasing),
            repeatMode = RepeatMode.Restart
        )
    )

    val outerCircleAlpha by infiniteTransition.animateFloat(
        initialValue = 0.3f,
        targetValue = 0f,
        animationSpec = infiniteRepeatable(
            animation = keyframes {
                durationMillis = 1500
                0.3f at 1200
                0f at 1500 with LinearEasing
            },
            repeatMode = RepeatMode.Restart
        )
    )

    Box(Modifier.size(diameter)) {
//        Outer Circle
        Box(
            modifier = Modifier
                .clip(CircleShape)
                .background(MaterialTheme.colors.primaryVariant.copy(alpha = outerCircleAlpha))
                .size(outerCircleSize.dp)
                .align(Alignment.Center),
        ) {}

//        Inner Circle
        Button(
            modifier = Modifier
                .padding(dimensionResource(id = R.dimen.padding_3x))
                .fillMaxSize(),
            shape = CircleShape,
            onClick = onClick,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = MaterialTheme.colors.primaryVariant
            )
        ) {
            Text(
                text = text,
                textAlign = TextAlign.Center,
                style = textStyle
            )
        }
    }
}

@Preview
@Composable
fun PreviewMnmRoundButton() {
    MnmRoundButton(
        text = "Go\nOnline",
        onClick = {}
    )
}