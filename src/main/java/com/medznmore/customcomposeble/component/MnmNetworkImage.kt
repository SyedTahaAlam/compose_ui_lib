package com.medznmore.customcomposeble.component

import android.media.Image
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import coil.compose.rememberImagePainter
import com.medznmore.customcomposeble.R

@Composable
fun MnmNetworkImage(
    modifier: Modifier = Modifier,
    url: String?,
    @DrawableRes placeholder: Int? = R.drawable.ic_no_image_place_holder,
    @DrawableRes fallback: Int? = R.drawable.ic_no_image_place_holder,
    @DrawableRes error: Int? = R.drawable.ic_no_image_place_holder,
    contentDescription: String? = null
){
    val url = "https://assets.medznmore.com/$url"
    Image(
        modifier = modifier,
        painter = rememberImagePainter(
            data = url,
            builder = {
                crossfade(true)
                placeholder?.let { placeholder(it) }
                fallback?.let { fallback(it) }
                error?.let { error(it) }
            }
        ),
        contentDescription = contentDescription
    )
}