package com.medznmore.customcomposeble.component

import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import com.medznmore.customcomposeble.R
import java.util.Locale

@Composable
fun MnmTextButton(
    modifier: Modifier = Modifier,
    text: String,
    onClick: () -> Unit,
    enabled: Boolean = true,
    showLoading: Boolean = false
) {
    TextButton(
        modifier = modifier
            .height(dimensionResource(id = R.dimen.button_height)),
        onClick = onClick,
        enabled = enabled,
        colors = ButtonDefaults.textButtonColors(
            contentColor = MaterialTheme.colors.secondary,
        )
    ) {
        if (showLoading) {
            CircularProgressIndicator(
                Modifier
                    .padding(dimensionResource(id = R.dimen.padding_0_25x))
                    .fillMaxHeight()
                    .aspectRatio(1f),
                color = MaterialTheme.colors.onPrimary,
                strokeWidth = dimensionResource(id = R.dimen.progress_bar_stroke_small)
            )
        } else {
            Text(text = text.uppercase(Locale.getDefault()))
        }
    }
}