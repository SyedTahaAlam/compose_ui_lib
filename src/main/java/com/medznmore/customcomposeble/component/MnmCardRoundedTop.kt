package com.medznmore.customcomposeble.component

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.Dp
import com.medznmore.customcomposeble.R

@Composable
fun MnmCardRoundedTop(
    modifier: Modifier = Modifier,
    cornerRadius: Dp = dimensionResource(id = R.dimen.radius_2x),
    elevation: Dp = dimensionResource(id = R.dimen.elevation_4x),
    backgroundColor: Color = MaterialTheme.colors.surface,
    content: @Composable () -> Unit
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(
            topStart = cornerRadius, topEnd = cornerRadius
        ),
        elevation = elevation,
        backgroundColor = backgroundColor
    ) {
        content()
    }
}