package com.medznmore.customcomposeble.component

import androidx.annotation.RawRes
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.font.FontWeight
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import com.medznmore.customcomposeble.R
import com.medznmore.customcomposeble.TextSecondary

@Composable
fun MnmInstructionCard(
    modifier: Modifier = Modifier,
    heading: String,
    body: String,
    buttonText: String,
    @RawRes animationResourceId: Int,
    onButtonClick: () -> Unit
) {
    MnmCardRoundedTop(
        modifier = modifier.fillMaxSize(),
        elevation = dimensionResource(id = R.dimen.elevation_0)
    ) {
        Column(Modifier.fillMaxSize()) {
            Column(
                Modifier
                    .fillMaxSize()
                    .padding(dimensionResource(id = R.dimen.padding_2x))
                    .weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                val composition by rememberLottieComposition(
                    LottieCompositionSpec.RawRes(animationResourceId)
                )
                LottieAnimation(
                    modifier = Modifier.size(dimensionResource(id = R.dimen.dimens_96)),
                    composition = composition,
                    iterations = LottieConstants.IterateForever
                )
                Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.padding_2x)))
                Text(
                    text = heading,
                    style = MaterialTheme.typography.h5.copy(fontWeight = FontWeight.Bold)
                )
                Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.padding_1_5x)))
                Text(
                    text = body,
                    style = MaterialTheme.typography.body1.copy(
                        color = TextSecondary
                    )
                )
            }
            MnmButton(
                text = buttonText,
                onClick = onButtonClick
            )
        }
    }
}