package com.medznmore.customcomposeble.component

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.ColorPainter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import com.medznmore.customcomposeble.R

@Composable
fun MnmRoundIconButton(
    radius: Dp = dimensionResource(id = R.dimen.radius_3x),
    painter: Painter,
    onClick: () -> Unit,
    backgroundColor: Color = MaterialTheme.colors.surface,
    iconTint: Color = MaterialTheme.colors.primaryVariant
) {
    val diameter = radius * 2
    Button(
        modifier = Modifier
            .size(diameter),
        shape = CircleShape,
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(
            backgroundColor = backgroundColor,
            contentColor = iconTint
        ),
        contentPadding = PaddingValues(dimensionResource(id = R.dimen.padding_1x)),
        elevation = ButtonDefaults.elevation(
            defaultElevation = dimensionResource(id = R.dimen.elevation_0),
            pressedElevation = dimensionResource(id = R.dimen.elevation_0)
        )
    ) {
        Icon(modifier = Modifier.fillMaxSize(), painter = painter, contentDescription = null)
    }
}

@Preview
@Composable
fun PreviewMnmRoundIconButton() {
    MnmRoundIconButton(
        painter = ColorPainter(Color.Blue),
        onClick = {}
    )
}