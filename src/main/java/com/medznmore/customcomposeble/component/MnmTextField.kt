package com.medznmore.customcomposeble.component

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.medznmore.customcomposeble.Constants
import com.medznmore.customcomposeble.PrefixTransformation

@Composable
fun MnmTextField(
    modifier: Modifier = Modifier,
    text: String,
    prefixText: String = Constants.EMPTY_STRING,
    label: String = Constants.EMPTY_STRING,
    onValueChange: (String) -> Unit,
    enabled: Boolean = true,
    maxLines: Int = 1,
    singleLine: Boolean = true,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    characterLimit: Int = Int.MAX_VALUE,
    isError: Boolean = false,
    errorMessage: String = Constants.EMPTY_STRING
) {
    Column(modifier = modifier.wrapContentSize(),) {
        OutlinedTextField(
            value = text,
            onValueChange = {
                if (it.length <= characterLimit) {
                    onValueChange(it)
                }
            },
            modifier = modifier.fillMaxWidth(),
            enabled = enabled,
            label = { Text(text = label) },
            maxLines = maxLines,
            singleLine = singleLine,
            keyboardOptions = keyboardOptions,
            keyboardActions = keyboardActions,
            visualTransformation = if (prefixText.isNotEmpty()) PrefixTransformation(prefixText) else VisualTransformation.None,
            isError = isError
        )
        if (isError && errorMessage.isNotEmpty())
            Text(
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(start = 16.dp),
                text = errorMessage
            )
    }
}



@Preview(showBackground = true)
@Composable
private fun Preview() {
  MnmTextField(
        text = "abc@gmail.com",
        label = "Email",
        onValueChange = {},
    )
}