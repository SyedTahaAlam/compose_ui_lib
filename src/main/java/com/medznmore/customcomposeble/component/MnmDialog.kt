package com.medznmore.customcomposeble.component

import androidx.compose.material.AlertDialog
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable

@Composable
fun MnmDialog(
    message: String,
    title: String? = null,
    positiveButton: String,
    positiveButtonClick: () -> Unit,
    negativeButton: String? = null,
    negativeButtonClick: () -> Unit = {},
    onDismissRequest: () -> Unit = {}
) {
    AlertDialog(
        onDismissRequest = onDismissRequest,
        text = {
            Text(
                text = message,
                style = MaterialTheme.typography.body1
            )
        },
        title = if (title != null) {
            {
                Text(text = title)
            }
        } else null,
        dismissButton = if (negativeButton != null) {
            {
                TextButton(onClick = negativeButtonClick) {
                    Text(text = negativeButton.uppercase())
                }
            }
        } else null,
        confirmButton = {
            TextButton(onClick = positiveButtonClick) {
                Text(text = positiveButton.uppercase())
            }
        }
    )
}