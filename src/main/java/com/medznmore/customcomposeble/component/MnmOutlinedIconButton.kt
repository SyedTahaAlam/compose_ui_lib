package com.medznmore.customcomposeble.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Dry
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.medznmore.customcomposeble.Neutral100
import com.medznmore.customcomposeble.R

@Composable
fun MnmOutlinedIconButton(
    modifier: Modifier = Modifier,
    painter: Painter,
    onClick: () -> Unit,
    strokeColor: Color = Neutral100,
    enabled: Boolean = true,
    iconTint: Color = Color.Black,
    backgroundColor: Color = Color.Transparent
) {
    OutlinedButton(
        modifier = modifier
            .height(dimensionResource(id = R.dimen.button_height))
            .wrapContentWidth(),
        onClick = onClick,
        enabled = enabled,
        elevation = ButtonDefaults.elevation(
            defaultElevation = dimensionResource(id = R.dimen.elevation_0),
            pressedElevation = dimensionResource(id = R.dimen.elevation_0)
        ),
        colors = ButtonDefaults.buttonColors(
            contentColor = iconTint,
            backgroundColor = backgroundColor,
            disabledBackgroundColor = Color.Transparent
        ),
        border = BorderStroke(2.dp, strokeColor)
    ) {
        Icon(painter = painter, contentDescription = null)
    }
}

@Preview
@Composable
fun PreviewMnmOutlinedIconButton() {
    MnmOutlinedIconButton(
        painter = rememberVectorPainter(image = Icons.Default.Dry),
        onClick = {}
    )
}