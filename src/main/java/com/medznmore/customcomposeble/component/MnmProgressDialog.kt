package com.medznmore.customcomposeble.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.medznmore.customcomposeble.R

@Composable
fun MnmProgressDialog(
    onDismissRequest: () -> Unit = {},
    text: String? = null,
) {
    Dialog(
        onDismissRequest = onDismissRequest,
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false),
    ) {
        Surface(
            modifier = Modifier.wrapContentSize(),
            shape = MaterialTheme.shapes.small
        ) {
            Row(
                modifier = Modifier.padding(
                    vertical = dimensionResource(id = R.dimen.padding_3x),
                    horizontal = if (text != null) {
                        dimensionResource(id = R.dimen.padding_7x)
                    } else {
                        dimensionResource(id = R.dimen.padding_3x)
                    }
                ),
                verticalAlignment = Alignment.CenterVertically
            ) {
                CircularProgressIndicator()
                if (text != null) {
                    Text(
                        modifier = Modifier.padding(dimensionResource(id = R.dimen.padding_1_5x)),
                        text = text
                    )
                }
            }
        }
    }
}