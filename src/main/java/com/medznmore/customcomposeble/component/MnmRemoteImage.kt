package com.medznmore.customcomposeble.component

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import coil.compose.rememberImagePainter

@Composable
fun MnmRemoteImage(
    modifier: Modifier = Modifier,
    url: String?,
    @DrawableRes placeholder: Int? = null,
    @DrawableRes fallback: Int? = null,
    @DrawableRes error: Int? = null,
    contentDescription: String? = null
) {
    Image(
        modifier = modifier,
        painter = rememberImagePainter(
            data = url,
            builder = {
                crossfade(true)
                placeholder?.let { placeholder(it) }
                fallback?.let { fallback(it) }
                error?.let { error(it) }
            }
        ),
        contentDescription = contentDescription
    )
}