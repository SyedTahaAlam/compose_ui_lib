package com.medznmore.customcomposeble.component

import androidx.compose.foundation.layout.Column
import androidx.compose.material.AlertDialog
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.font.FontWeight
import com.medznmore.customcomposeble.Constants

@Composable
fun MnmTextFieldDialog(
    message: String,
    title: String? = null,
    positiveButton: String,
    positiveButtonClick: (String) -> Unit,
    negativeButton: String?,
    negativeButtonClick: () -> Unit = {},
    onDismissRequest: () -> Unit = {},
    textFieldDefaultText: String = Constants.EMPTY_STRING
) {
    var textFieldText: String by remember { mutableStateOf(textFieldDefaultText) }

    AlertDialog(
        onDismissRequest = onDismissRequest,
        text = {
            CompositionLocalProvider(
                LocalTextStyle provides MaterialTheme.typography.body1.copy(
                    fontWeight = FontWeight.SemiBold
                )
            ) {
                Column {
                    Text(text = message)
                    MnmTextField(text = textFieldText, onValueChange = { textFieldText = it })
                }
            }
        },
        title = if (title != null) {
            { Text(text = title) }
        } else null,
        dismissButton = if (negativeButton != null) {
            {
                TextButton(onClick = negativeButtonClick) {
                    Text(text = negativeButton.uppercase())
                }
            }
        } else null,
        confirmButton = {
            TextButton(
                onClick = {
                    positiveButtonClick(textFieldText)
                }
            ) {
                Text(text = positiveButton.uppercase())
            }
        }
    )
}